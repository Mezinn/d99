-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 10, 2018 at 03:29 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.2.10-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `d99`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passport_code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `room_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `birthday_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `surname`, `passport_code`, `description`, `room_id`, `created_at`, `updated_at`, `birthday_date`) VALUES
(2, 'Боря', 'Таранька', '228', '', 2, '2018-11-10 12:49:34', '2018-11-10 13:27:22', '2018-11-10'),
(3, 'В', 'В', '2352345245', '', 1, '2018-11-10 13:24:19', '2018-11-10 13:27:08', '2018-11-10');

-- --------------------------------------------------------

--
-- Table structure for table `member_affairs`
--

CREATE TABLE `member_affairs` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  `is_valid_till` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `member_logs`
--

CREATE TABLE `member_logs` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `information` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `room_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `member_logs`
--

INSERT INTO `member_logs` (`id`, `member_id`, `information`, `created_at`, `room_number`) VALUES
(8, 2, 'Заселення', '2018-11-10 12:49:37', 28),
(9, 2, 'Виселення', '2018-11-10 12:50:00', 28),
(10, 2, 'Заселення', '2018-11-10 12:50:03', 28),
(11, 2, 'Виселення', '2018-11-10 12:50:35', 28),
(12, 2, 'Заселення', '2018-11-10 12:50:38', 28),
(13, 2, 'Виселення', '2018-11-10 12:51:07', 28),
(14, 2, 'Заселення', '2018-11-10 12:51:37', 28),
(15, 2, 'Виселення', '2018-11-10 12:51:59', 28),
(16, 2, 'Заселення', '2018-11-10 12:52:02', 28),
(17, 2, 'Переселення', '2018-11-10 12:52:36', 25),
(18, 2, 'Переселення', '2018-11-10 12:53:07', 25),
(19, 2, 'Переселення', '2018-11-10 12:58:18', 25),
(20, 2, 'Переселення', '2018-11-10 12:58:26', 25),
(21, 2, 'Переселення', '2018-11-10 12:58:34', 25),
(22, 2, 'Переселення', '2018-11-10 13:01:30', 25),
(23, 2, 'Переселення', '2018-11-10 13:02:03', 25),
(24, 2, 'Переселення', '2018-11-10 13:03:21', 25),
(25, 2, 'Переселення', '2018-11-10 13:04:00', 25),
(26, 2, 'Переселення', '2018-11-10 13:04:20', 25),
(27, 2, 'Виселення', '2018-11-10 13:09:26', 25),
(28, 3, 'Заселення', '2018-11-10 13:24:23', 28),
(29, 3, 'Переселення', '2018-11-10 13:27:02', 28),
(30, 3, 'Переселення', '2018-11-10 13:27:05', 25),
(31, 3, 'Переселення', '2018-11-10 13:27:08', 28),
(32, 2, 'Заселення', '2018-11-10 13:27:22', 25);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `floor` int(11) NOT NULL,
  `max_members` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `number`, `floor`, `max_members`, `description`, `created_at`, `updated_at`) VALUES
(1, 28, 3, 1, '', '2018-11-08 20:24:57', '2018-11-10 12:48:55'),
(2, 25, 4, 7, '', '2018-11-10 12:27:27', '2018-11-10 12:27:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `passport_code` (`passport_code`),
  ADD KEY `room_id` (`room_id`);

--
-- Indexes for table `member_affairs`
--
ALTER TABLE `member_affairs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `member_logs`
--
ALTER TABLE `member_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `member_affairs`
--
ALTER TABLE `member_affairs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member_logs`
--
ALTER TABLE `member_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`);

--
-- Constraints for table `member_affairs`
--
ALTER TABLE `member_affairs`
  ADD CONSTRAINT `member_affairs_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `member_logs`
--
ALTER TABLE `member_logs`
  ADD CONSTRAINT `member_logs_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
