<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rooms".
 *
 * @property int $id
 * @property int $number
 * @property int $floor
 * @property int $max_members
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Members[] $members
 */
class Rooms extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'rooms';
    }

    public function behaviors() {
        return[
            'timestamps' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function rules() {
        return [
            [['number', 'floor', 'max_members'], 'required'],
            [['number', 'floor', 'max_members'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'floor' => 'Поверх',
            'max_members' => 'Максимальна кількість жителів',
            'description' => 'Додаткова інформація',
            'created_at' => 'Створено',
            'updated_at' => 'Відредаговано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers() {
        return $this->hasMany(Members::className(), ['room_id' => 'id']);
    }

    public static function getList($with_out = 0) {
        return ArrayHelper::map(self::find()->where(['not', ['id' => $with_out]])->all(), 'id', 'number');
    }

}
