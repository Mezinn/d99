<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_logs".
 *
 * @property int $id
 * @property int $member_id
 * @property string $information
 * @property string $created_at
 *
 * @property Members $member
 */
class MemberLogs extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'member_logs';
    }

    public function rules() {
        return [
            [['member_id', 'information'], 'required'],
            [['member_id', 'room_number'], 'integer'],
            [['created_at'], 'safe'],
            [['information'], 'string', 'max' => 255],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => Members::className(), 'targetAttribute' => ['member_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'information' => 'Інформація',
            'created_at' => 'Дата/час',
            'room_number' => 'Номер кімнати',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember() {
        return $this->hasOne(Members::className(), ['id' => 'member_id']);
    }

}
