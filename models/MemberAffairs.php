<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_affairs".
 *
 * @property int $id
 * @property int $member_id
 * @property int $paid
 * @property string $is_valid_till
 *
 * @property Members $member
 */
class MemberAffairs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member_affairs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'paid', 'is_valid_till'], 'required'],
            [['member_id', 'paid'], 'integer'],
            [['is_valid_till'], 'safe'],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => Members::className(), 'targetAttribute' => ['member_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'paid' => 'Paid',
            'is_valid_till' => 'Is Valid Till',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Members::className(), ['id' => 'member_id']);
    }
}
