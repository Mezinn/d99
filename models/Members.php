<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "members".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $passport_code
 * @property string $description
 * @property int $room_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $birthday_date
 *
 * @property MemberAffairs[] $memberAffairs
 * @property MemberLogs[] $memberLogs
 * @property Rooms $room
 */
class Members extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'members';
    }

    public function behaviors() {
        return [
            'timestamps' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function rules() {
        return [
            [['name', 'surname', 'passport_code', 'birthday_date'], 'required'],
            [['description'], 'string'],
            [['room_id'], 'integer'],
            [['created_at', 'updated_at', 'birthday_date'], 'safe'],
            [['name', 'surname'], 'string', 'max' => 255],
            [['passport_code'], 'string', 'max' => 32],
            [['passport_code'], 'unique'],
            [['room_id'], 'check_free_space'],
            [['room_id'], 'exist', 'targetClass' => Rooms::className(), 'targetAttribute' => ['room_id' => 'id']],
        ];
    }

    public function check_free_space($attribute, $param, $validator) {
        $model = Rooms::findOne($this->{$attribute});
        if ($model) {
            $current_count = Members::find()->where(['room_id' => $this->{$attribute}])->andWhere(['not', ['id' => $this->id]])->count();
            if ($current_count >= $model->max_members) {
                $this->{$attribute} = $this->getOldAttribute($attribute);
                $this->addError($attribute, 'У кімнаті недостатньо місця');
                return;
            }
        }
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Ім\'я',
            'surname' => 'Прізвище',
            'passport_code' => 'Номер паспорту',
            'description' => 'Додаткова інформація',
            'room_id' => 'Номер кімнати',
            'created_at' => 'Стоворено',
            'updated_at' => 'Відредаговано',
            'birthday_date' => 'Дата народження',
            'fullName' => 'Ім\'я/Прізвище'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberAffairs() {
        return $this->hasMany(MemberAffairs::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberLogs() {
        return $this->hasMany(MemberLogs::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom() {
        return $this->hasOne(Rooms::className(), ['id' => 'room_id']);
    }

    public function getFullName() {
        return "$this->name $this->surname";
    }

    public function getLogsDataProvider() {
        return new ActiveDataProvider(['query' => MemberLogs::find()->where(['member_id' => $this->id])]);
    }

}
