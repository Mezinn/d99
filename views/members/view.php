<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Members */

$this->title = $model->fullName;
$this->params['breadcrumbs'][] = ['label' => 'Жителі', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="members-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Ви впевнені, що хочете видалити цей елемент?',
                'method' => 'post',
            ],
        ])
        ?>
        <?php if (empty($model->room_id)): ?>
            <?= Html::a('Заселити', ['populate', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php else : ?>
            <?= Html::a('Переселити', ['move', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Виселити', ['evict', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Ви впевнені?',
                    'method' => 'post',
                ],
            ])
            ?>
        <?php endif; ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fullName',
            'surname',
            'passport_code',
            'description:ntext',
            'room.number',
            'created_at',
            'updated_at',
            'birthday_date',
        ],
    ])
    ?>

</div>
<?=
GridView::widget(['dataProvider' => $model->getLogsDataProvider(),
    'columns' => [
        'information',
        'room_number',
        'created_at',
    ],
])
?>
