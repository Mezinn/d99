<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Rooms;
?>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model->room, 'number')->textInput(['disabled' => true]) ?>
<?= $form->field($model, 'room_id')->dropDownList(Rooms::getList($model->room_id)) ?>


<p>
    <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
</p>

<?php ActiveForm::end(); ?>