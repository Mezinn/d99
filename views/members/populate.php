<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Rooms;
?>


<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'room_id')->dropDownList(Rooms::getList()) ?>
<p>
    <?=Html::submitButton('Зберегти',['class'=>'btn btn-success'])?>
</p>

<?php ActiveForm::end(); ?>