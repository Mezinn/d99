<?php

namespace app\controllers;

use Yii;
use app\models\Members;
use app\models\search\MembersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
use app\models\MemberLogs;
use yii\filters\AccessControl;

/**
 * MembersController implements the CRUD actions for Members model.
 */
class MembersController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Members models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MembersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Members model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Members model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Members();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionPopulate($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) and $model->save()) {
            (new MemberLogs(['member_id' => $model->id, 'information' => 'Заселення', 'created_at' => new Expression('NOW()'), 'room_number' => $model->room->number]))->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('populate', ['model' => $model]);
    }

    /**
     * Updates an existing Members model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Members model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Members model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Members the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Members::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionEvict($id) {
        $model = $this->findModel($id);
        (new MemberLogs(['member_id' => $model->id, 'information' => 'Виселення', 'created_at' => new Expression('NOW()'), 'room_number' => $model->room->number]))->save();
        $model->room_id = null;
        $model->save(false);
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionMove($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) and $model->validate()) {
            if (Yii::$app->getDb()->transaction(function()use($model) {
                        return $model->save()and ( new MemberLogs(['member_id' => $model->id, 'information' => 'Переселення', 'created_at' => new Expression('NOW()'), 'room_number' => $model->room->number]))->save();
                    })) {
                return $this->redirect(['view', 'id' => $id]);
            }
        }
        return $this->render('move', ['model' => $model]);
    }

}
